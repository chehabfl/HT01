TARGET=\
	   main.pdf

GENERATED_TEX= \
		  src/titlePage.tex \
		  src/content.tex \

GENERATED= $(GENERATED_TEX)

PDFLATEX=pdflatex -shell-escape -halt-on-error

main.pdf: main.tex $(GENERATED)
	$(PDFLATEX) main.tex
	bibtex main.aux
	$(PDFLATEX) main.tex
	$(PDFLATEX) main.tex

.SECONDARY: $(GENERATED)

.PHONY: clean mrproper

clean:
	rm -f \
		$(TARGET:%.pdf=%.aux) \
		$(TARGET:%.pdf=%.out) \
		$(TARGET:%.pdf=%.tdo) \
		$(TARGET:%.pdf=%.toc) \
		$(TARGET:%.pdf=%.log) \
		$(TARGET:%.pdf=%.bbl) \
		$(TARGET:%.pdf=%.blg) \
		$(TARGET:%.pdf=%.fls) \
		$(TARGET:%.pdf=%.run.xml) \
		$(TARGET:%.pdf=%-blx.bib) \

mrproper: clean
	rm -f \
		$(TARGET) \
