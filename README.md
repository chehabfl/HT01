![build](/../badges/master/build.svg)

# Mémoire de l'UV HT01. _La Cryptologie : analyse de la course entre cryptographie et cryptanalyse._

## Pour télécharger le `PDF` rendez-vous [ici 📄](https://chehabfl.gitlab.utc.fr/HT01/HT01__La_Cryptologie.pdf).

😃 Bonne lecture ! 😃


**Licence :**

Le contenu de ce document ainsi que les éléments contenus dans le fichier `src/content.tex` sont mis à disposition sous les termes de la [Licence Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/). 